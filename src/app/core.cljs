(ns app.core
  (:require [reagent.dom :as rdom]))


(defn app []
  [:div
   [:h1 "Clojurescript - Shadow-cljs - starter"]])

(defn ^:export init []
  (rdom/render [app] (.getElementById js/document "app")))
